import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'task' })
export class TaskEntity {

    // @ApiProperty()
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @ApiProperty()
    @Column()
    title: string;

    @ApiProperty()
    @Column()
    description: string

    @ApiProperty()
    @Column({
        type: 'boolean',
        default: false
    })
    is_done: boolean

    // @ApiProperty()
    @CreateDateColumn()
    create_at: Date

    // @ApiProperty()
    @UpdateDateColumn()
    updated_at: Date

}
