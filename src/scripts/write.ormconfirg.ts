import { DatabaseConnectionService } from '../shared/services/database-connection.service.service';
import fs = require('fs');

const ormConfig = new DatabaseConnectionService();

fs.writeFileSync('ormconfig.json',
    JSON.stringify(ormConfig.createTypeOrmOptions(), null, 2)
);
