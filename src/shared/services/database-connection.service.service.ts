import { Injectable } from '@nestjs/common';
import { TypeOrmOptionsFactory, TypeOrmModuleOptions } from '@nestjs/typeorm';
import 'dotenv/config';

@Injectable()
export class DatabaseConnectionService implements TypeOrmOptionsFactory {

    public createTypeOrmOptions(): TypeOrmModuleOptions {
        return {
            // name: 'default',
            type: 'mysql',
            host: process.env.DATABASE_HOST,
            port: Number(process.env.DATABASE_PORT),
            username: process.env.DATABASE_USER,
            password: process.env.DATABASE_PASSWORD,
            database: process.env.DATABASE_DB,
            // synchronize: false,
            // dropSchema: false,
            // logging: true,
            entities: ['dist/models/task.entity.js'],
            migrations: ['src/migration/*.ts'],
            cli: {
                migrationsDir: 'src/migration'
            },
            migrationsTableName: 'migration',
            ssl: false
        }
    }
}