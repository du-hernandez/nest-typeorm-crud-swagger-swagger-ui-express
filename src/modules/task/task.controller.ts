import { Controller } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud'
import { TaskEntity } from '../../models/task.entity';
import { TaskService } from './task.service';

@Crud({
    model: {
        type: TaskEntity
    },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true
        }
    }
})
@Controller('task')
export class TaskController implements CrudController<TaskEntity> {

    constructor(public service: TaskService) { }

}
