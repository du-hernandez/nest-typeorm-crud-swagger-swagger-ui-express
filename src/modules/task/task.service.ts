import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { InjectRepository } from '@nestjs/typeorm';;
import { TaskEntity } from '../../models/task.entity';
import { Repository } from 'typeorm';

@Injectable()
export class TaskService extends TypeOrmCrudService<TaskEntity> {

    constructor(@InjectRepository(TaskEntity) repo: Repository<TaskEntity>) {
        super(repo);
    }

}
