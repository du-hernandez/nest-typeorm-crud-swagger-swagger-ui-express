import { Module } from '@nestjs/common';
// import * as dotenv from 'dotenv';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseConnectionService } from './shared/services/database-connection.service.service';
import { TaskModule } from './modules/task/task.module';

// dotenv.config();

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      useClass: DatabaseConnectionService
    }),
    TaskModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
